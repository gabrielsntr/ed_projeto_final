/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author GabrielBuosi
 */
public class CalculoHoras {
    
    Conexao conexao = new Conexao();
    
    
    public float auxPreviaHorasCertificado(int idCrit){
        ResultSet rs;
        
    String sql = "select * from criterioslocaltempo where idCriteriosLocalTempo = "+idCrit+";";
    
        System.out.println(sql);
        conexao.conectar();
        try {
            rs =conexao.pesquisa(sql);
            rs.first();
            int idRegraHoras = rs.getInt("RegraDeHoras_idRegraDeHoras");
            return previaHorasCertificado(idRegraHoras);
        } catch (SQLException ex) {
            return -1;
        } finally {
            conexao.desconectar();
        }
    }
    
    public float previaHorasCertificado(int idRegraHoras){
        ResultSet rs;
        
    String sql = "select * from regradehoras where idRegraDeHoras = "+idRegraHoras+";";
        System.out.println(sql);
        conexao.conectar();
        try {
            rs =conexao.pesquisa(sql);
            rs.first();
            float valorHoras = rs.getFloat("cargaHorariaMaxima");
            return valorHoras;
        } catch (SQLException ex) {
            return 0;
        } finally {
            conexao.desconectar();
        }
    }
    
}
