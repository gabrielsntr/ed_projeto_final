/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import com.sun.javafx.binding.StringFormatter;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author GabrielBuosi
 */
public class Utils {
    
    public static void centralizarJanela(Window frame){
        Dimension dimensao = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimensao.getWidth() - frame.getWidth()) / 2 );
        int y = (int) ((dimensao.getHeight() - frame.getHeight()) / 2 );
        frame.setLocation(x, y);
    }
    
    public static Date stringToDate(String data) throws ParseException{
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        return formato.parse(data);
    }
    
    public static String DateToStringSQL(java.util.Date data){
        Format formatar = new SimpleDateFormat("yyyy-MM-dd");
        return formatar.format(data);
    }
    
    
    public static void mascaraData(JFormattedTextField tf) throws ParseException{
        MaskFormatter mascara = new MaskFormatter("##/##/####");
        mascara.install(tf);
    }
    
}
