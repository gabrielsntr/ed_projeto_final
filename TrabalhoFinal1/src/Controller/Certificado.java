/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
//import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author GabrielBuosi
 */
@Entity
@Table(name = "certificado", catalog = "trabalhofinal", schema = "")
@NamedQueries({
    @NamedQuery(name = "Certificado.findAll", query = "SELECT c FROM Certificado c")
    , @NamedQuery(name = "Certificado.findByIdCertificado", query = "SELECT c FROM Certificado c WHERE c.idCertificado = :idCertificado")
    , @NamedQuery(name = "Certificado.findByAlunoidAluno", query = "SELECT c FROM Certificado c WHERE c.alunoidAluno = :alunoidAluno")
    , @NamedQuery(name = "Certificado.findByTitulo", query = "SELECT c FROM Certificado c WHERE c.titulo = :titulo")
    , @NamedQuery(name = "Certificado.findByDataEnvio", query = "SELECT c FROM Certificado c WHERE c.dataEnvio = :dataEnvio")
    , @NamedQuery(name = "Certificado.findByDataEmissao", query = "SELECT c FROM Certificado c WHERE c.dataEmissao = :dataEmissao")
    , @NamedQuery(name = "Certificado.findByHorasValidas", query = "SELECT c FROM Certificado c WHERE c.horasValidas = :horasValidas")
    , @NamedQuery(name = "Certificado.findByCargaHoraria", query = "SELECT c FROM Certificado c WHERE c.cargaHoraria = :cargaHoraria")
    , @NamedQuery(name = "Certificado.findByComentario", query = "SELECT c FROM Certificado c WHERE c.comentario = :comentario")
    , @NamedQuery(name = "Certificado.findByCategoriaCertificadoidCategoriaCertificado", query = "SELECT c FROM Certificado c WHERE c.categoriaCertificadoidCategoriaCertificado = :categoriaCertificadoidCategoriaCertificado")
    , @NamedQuery(name = "Certificado.findBySubCategoriaCertificadoidSubCategoriaCertificado", query = "SELECT c FROM Certificado c WHERE c.subCategoriaCertificadoidSubCategoriaCertificado = :subCategoriaCertificadoidSubCategoriaCertificado")
    , @NamedQuery(name = "Certificado.findByCriteriosLocalTempoidCriteriosLocalTempo", query = "SELECT c FROM Certificado c WHERE c.criteriosLocalTempoidCriteriosLocalTempo = :criteriosLocalTempoidCriteriosLocalTempo")
    , @NamedQuery(name = "Certificado.findByCaminhoImagem", query = "SELECT c FROM Certificado c WHERE c.caminhoImagem = :caminhoImagem")})
public class Certificado implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCertificado")
    private Integer idCertificado;
    @Basic(optional = false)
    @Column(name = "Aluno_idAluno")
    private int alunoidAluno;
    @Basic(optional = false)
    @Column(name = "titulo")
    private String titulo;
    @Basic(optional = false)
    @Column(name = "dataEnvio")
    
    private String dataEnvio;
    @Column(name = "dataEmissao")
    private String dataEmissao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "horasValidas")
    private Float horasValidas;
    @Basic(optional = false)
    @Column(name = "cargaHoraria")
    private float cargaHoraria;
    @Column(name = "comentario")
    private String comentario;
    @Basic(optional = false)
    @Column(name = "CategoriaCertificado_idCategoriaCertificado")
    private int categoriaCertificadoidCategoriaCertificado;
    @Basic(optional = false)
    @Column(name = "SubCategoriaCertificado_idSubCategoriaCertificado")
    private int subCategoriaCertificadoidSubCategoriaCertificado;
    @Basic(optional = false)
    @Column(name = "CriteriosLocalTempo_idCriteriosLocalTempo")
    private int criteriosLocalTempoidCriteriosLocalTempo;
    @Column(name = "caminhoImagem")
    private String caminhoImagem;

    public Certificado() {
    }

    public Certificado(Integer idCertificado) {
        this.idCertificado = idCertificado;
    }

    public Certificado(Integer idCertificado, int alunoidAluno, String titulo, String dataEnvio, float cargaHoraria, int categoriaCertificadoidCategoriaCertificado, int subCategoriaCertificadoidSubCategoriaCertificado, int criteriosLocalTempoidCriteriosLocalTempo) {
        this.idCertificado = idCertificado;
        this.alunoidAluno = alunoidAluno;
        this.titulo = titulo;
        this.dataEnvio = dataEnvio;
        this.cargaHoraria = cargaHoraria;
        this.categoriaCertificadoidCategoriaCertificado = categoriaCertificadoidCategoriaCertificado;
        this.subCategoriaCertificadoidSubCategoriaCertificado = subCategoriaCertificadoidSubCategoriaCertificado;
        this.criteriosLocalTempoidCriteriosLocalTempo = criteriosLocalTempoidCriteriosLocalTempo;
    }

    public Integer getIdCertificado() {
        return idCertificado;
    }

    public void setIdCertificado(Integer idCertificado) {
        Integer oldIdCertificado = this.idCertificado;
        this.idCertificado = idCertificado;
        changeSupport.firePropertyChange("idCertificado", oldIdCertificado, idCertificado);
    }

    public int getAlunoidAluno() {
        return alunoidAluno;
    }

    public void setAlunoidAluno(int alunoidAluno) {
        int oldAlunoidAluno = this.alunoidAluno;
        this.alunoidAluno = alunoidAluno;
        changeSupport.firePropertyChange("alunoidAluno", oldAlunoidAluno, alunoidAluno);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        String oldTitulo = this.titulo;
        this.titulo = titulo;
        changeSupport.firePropertyChange("titulo", oldTitulo, titulo);
    }

    public String getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(String dataEnvio) {
        String oldDataEnvio = this.dataEnvio;
        this.dataEnvio = dataEnvio;
        changeSupport.firePropertyChange("dataEnvio", oldDataEnvio, dataEnvio);
    }

    public String getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(String dataEmissao) {
        String oldDataEmissao = this.dataEmissao;
        this.dataEmissao = dataEmissao;
        changeSupport.firePropertyChange("dataEmissao", oldDataEmissao, dataEmissao);
    }

    public Float getHorasValidas() {
        return horasValidas;
    }

    public void setHorasValidas(Float horasValidas) {
        Float oldHorasValidas = this.horasValidas;
        this.horasValidas = horasValidas;
        changeSupport.firePropertyChange("horasValidas", oldHorasValidas, horasValidas);
    }

    public float getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(float cargaHoraria) {
        float oldCargaHoraria = this.cargaHoraria;
        this.cargaHoraria = cargaHoraria;
        changeSupport.firePropertyChange("cargaHoraria", oldCargaHoraria, cargaHoraria);
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        String oldComentario = this.comentario;
        this.comentario = comentario;
        changeSupport.firePropertyChange("comentario", oldComentario, comentario);
    }

    public int getCategoriaCertificadoidCategoriaCertificado() {
        return categoriaCertificadoidCategoriaCertificado;
    }

    public void setCategoriaCertificadoidCategoriaCertificado(int categoriaCertificadoidCategoriaCertificado) {
        int oldCategoriaCertificadoidCategoriaCertificado = this.categoriaCertificadoidCategoriaCertificado;
        this.categoriaCertificadoidCategoriaCertificado = categoriaCertificadoidCategoriaCertificado;
        changeSupport.firePropertyChange("categoriaCertificadoidCategoriaCertificado", oldCategoriaCertificadoidCategoriaCertificado, categoriaCertificadoidCategoriaCertificado);
    }

    public int getSubCategoriaCertificadoidSubCategoriaCertificado() {
        return subCategoriaCertificadoidSubCategoriaCertificado;
    }

    public void setSubCategoriaCertificadoidSubCategoriaCertificado(int subCategoriaCertificadoidSubCategoriaCertificado) {
        int oldSubCategoriaCertificadoidSubCategoriaCertificado = this.subCategoriaCertificadoidSubCategoriaCertificado;
        this.subCategoriaCertificadoidSubCategoriaCertificado = subCategoriaCertificadoidSubCategoriaCertificado;
        changeSupport.firePropertyChange("subCategoriaCertificadoidSubCategoriaCertificado", oldSubCategoriaCertificadoidSubCategoriaCertificado, subCategoriaCertificadoidSubCategoriaCertificado);
    }

    public int getCriteriosLocalTempoidCriteriosLocalTempo() {
        return criteriosLocalTempoidCriteriosLocalTempo;
    }

    public void setCriteriosLocalTempoidCriteriosLocalTempo(int criteriosLocalTempoidCriteriosLocalTempo) {
        int oldCriteriosLocalTempoidCriteriosLocalTempo = this.criteriosLocalTempoidCriteriosLocalTempo;
        this.criteriosLocalTempoidCriteriosLocalTempo = criteriosLocalTempoidCriteriosLocalTempo;
        changeSupport.firePropertyChange("criteriosLocalTempoidCriteriosLocalTempo", oldCriteriosLocalTempoidCriteriosLocalTempo, criteriosLocalTempoidCriteriosLocalTempo);
    }

    public String getCaminhoImagem() {
        return caminhoImagem;
    }

    public void setCaminhoImagem(String caminhoImagem) {
        String oldCaminhoImagem = this.caminhoImagem;
        this.caminhoImagem = caminhoImagem;
        changeSupport.firePropertyChange("caminhoImagem", oldCaminhoImagem, caminhoImagem);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCertificado != null ? idCertificado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Certificado)) {
            return false;
        }
        Certificado other = (Certificado) object;
        if ((this.idCertificado == null && other.idCertificado != null) || (this.idCertificado != null && !this.idCertificado.equals(other.idCertificado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "View.Certificado[ idCertificado=" + idCertificado + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
