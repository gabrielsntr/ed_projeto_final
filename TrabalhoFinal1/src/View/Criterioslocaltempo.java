/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author GabrielBuosi
 */
@Entity
@Table(name = "criterioslocaltempo", catalog = "trabalhofinal", schema = "")
@NamedQueries({
    @NamedQuery(name = "Criterioslocaltempo.findAll", query = "SELECT c FROM Criterioslocaltempo c")
    , @NamedQuery(name = "Criterioslocaltempo.findByIdCriteriosLocalTempo", query = "SELECT c FROM Criterioslocaltempo c WHERE c.idCriteriosLocalTempo = :idCriteriosLocalTempo")
    , @NamedQuery(name = "Criterioslocaltempo.findByNome", query = "SELECT c FROM Criterioslocaltempo c WHERE c.nome = :nome")
    , @NamedQuery(name = "Criterioslocaltempo.findByRegraDeHorasidRegraDeHoras", query = "SELECT c FROM Criterioslocaltempo c WHERE c.regraDeHorasidRegraDeHoras = :regraDeHorasidRegraDeHoras")
    , @NamedQuery(name = "Criterioslocaltempo.findBySubCategoriaCertificadoidSubCategoriaCertificado", query = "SELECT c FROM Criterioslocaltempo c WHERE c.subCategoriaCertificadoidSubCategoriaCertificado = :subCategoriaCertificadoidSubCategoriaCertificado")})
public class Criterioslocaltempo implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCriteriosLocalTempo")
    private Integer idCriteriosLocalTempo;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "RegraDeHoras_idRegraDeHoras")
    private int regraDeHorasidRegraDeHoras;
    @Basic(optional = false)
    @Column(name = "SubCategoriaCertificado_idSubCategoriaCertificado")
    private int subCategoriaCertificadoidSubCategoriaCertificado;

    public Criterioslocaltempo() {
    }

    public Criterioslocaltempo(Integer idCriteriosLocalTempo) {
        this.idCriteriosLocalTempo = idCriteriosLocalTempo;
    }

    public Criterioslocaltempo(Integer idCriteriosLocalTempo, String nome, int regraDeHorasidRegraDeHoras, int subCategoriaCertificadoidSubCategoriaCertificado) {
        this.idCriteriosLocalTempo = idCriteriosLocalTempo;
        this.nome = nome;
        this.regraDeHorasidRegraDeHoras = regraDeHorasidRegraDeHoras;
        this.subCategoriaCertificadoidSubCategoriaCertificado = subCategoriaCertificadoidSubCategoriaCertificado;
    }

    public Integer getIdCriteriosLocalTempo() {
        return idCriteriosLocalTempo;
    }

    public void setIdCriteriosLocalTempo(Integer idCriteriosLocalTempo) {
        Integer oldIdCriteriosLocalTempo = this.idCriteriosLocalTempo;
        this.idCriteriosLocalTempo = idCriteriosLocalTempo;
        changeSupport.firePropertyChange("idCriteriosLocalTempo", oldIdCriteriosLocalTempo, idCriteriosLocalTempo);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        String oldNome = this.nome;
        this.nome = nome;
        changeSupport.firePropertyChange("nome", oldNome, nome);
    }

    public int getRegraDeHorasidRegraDeHoras() {
        return regraDeHorasidRegraDeHoras;
    }

    public void setRegraDeHorasidRegraDeHoras(int regraDeHorasidRegraDeHoras) {
        int oldRegraDeHorasidRegraDeHoras = this.regraDeHorasidRegraDeHoras;
        this.regraDeHorasidRegraDeHoras = regraDeHorasidRegraDeHoras;
        changeSupport.firePropertyChange("regraDeHorasidRegraDeHoras", oldRegraDeHorasidRegraDeHoras, regraDeHorasidRegraDeHoras);
    }

    public int getSubCategoriaCertificadoidSubCategoriaCertificado() {
        return subCategoriaCertificadoidSubCategoriaCertificado;
    }

    public void setSubCategoriaCertificadoidSubCategoriaCertificado(int subCategoriaCertificadoidSubCategoriaCertificado) {
        int oldSubCategoriaCertificadoidSubCategoriaCertificado = this.subCategoriaCertificadoidSubCategoriaCertificado;
        this.subCategoriaCertificadoidSubCategoriaCertificado = subCategoriaCertificadoidSubCategoriaCertificado;
        changeSupport.firePropertyChange("subCategoriaCertificadoidSubCategoriaCertificado", oldSubCategoriaCertificadoidSubCategoriaCertificado, subCategoriaCertificadoidSubCategoriaCertificado);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCriteriosLocalTempo != null ? idCriteriosLocalTempo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Criterioslocaltempo)) {
            return false;
        }
        Criterioslocaltempo other = (Criterioslocaltempo) object;
        if ((this.idCriteriosLocalTempo == null && other.idCriteriosLocalTempo != null) || (this.idCriteriosLocalTempo != null && !this.idCriteriosLocalTempo.equals(other.idCriteriosLocalTempo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "View.Criterioslocaltempo[ idCriteriosLocalTempo=" + idCriteriosLocalTempo + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
