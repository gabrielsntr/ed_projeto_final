/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author GabrielBuosi
 */
@Entity
@Table(name = "subcategoriacertificado", catalog = "trabalhofinal", schema = "")
@NamedQueries({
    @NamedQuery(name = "Subcategoriacertificado.findAll", query = "SELECT s FROM Subcategoriacertificado s")
    , @NamedQuery(name = "Subcategoriacertificado.findByIdSubCategoriaCertificado", query = "SELECT s FROM Subcategoriacertificado s WHERE s.idSubCategoriaCertificado = :idSubCategoriaCertificado")
    , @NamedQuery(name = "Subcategoriacertificado.findByNome", query = "SELECT s FROM Subcategoriacertificado s WHERE s.nome = :nome")
    , @NamedQuery(name = "Subcategoriacertificado.findByCategoriaCertificadoidCategoriaCertificado", query = "SELECT s FROM Subcategoriacertificado s WHERE s.categoriaCertificadoidCategoriaCertificado = :categoriaCertificadoidCategoriaCertificado")})
public class Subcategoriacertificado implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idSubCategoriaCertificado")
    private Integer idSubCategoriaCertificado;
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "CategoriaCertificado_idCategoriaCertificado")
    private int categoriaCertificadoidCategoriaCertificado;

    public Subcategoriacertificado() {
    }

    public Subcategoriacertificado(Integer idSubCategoriaCertificado) {
        this.idSubCategoriaCertificado = idSubCategoriaCertificado;
    }

    public Subcategoriacertificado(Integer idSubCategoriaCertificado, int categoriaCertificadoidCategoriaCertificado) {
        this.idSubCategoriaCertificado = idSubCategoriaCertificado;
        this.categoriaCertificadoidCategoriaCertificado = categoriaCertificadoidCategoriaCertificado;
    }

    public Integer getIdSubCategoriaCertificado() {
        return idSubCategoriaCertificado;
    }

    public void setIdSubCategoriaCertificado(Integer idSubCategoriaCertificado) {
        Integer oldIdSubCategoriaCertificado = this.idSubCategoriaCertificado;
        this.idSubCategoriaCertificado = idSubCategoriaCertificado;
        changeSupport.firePropertyChange("idSubCategoriaCertificado", oldIdSubCategoriaCertificado, idSubCategoriaCertificado);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        String oldNome = this.nome;
        this.nome = nome;
        changeSupport.firePropertyChange("nome", oldNome, nome);
    }

    public int getCategoriaCertificadoidCategoriaCertificado() {
        return categoriaCertificadoidCategoriaCertificado;
    }

    public void setCategoriaCertificadoidCategoriaCertificado(int categoriaCertificadoidCategoriaCertificado) {
        int oldCategoriaCertificadoidCategoriaCertificado = this.categoriaCertificadoidCategoriaCertificado;
        this.categoriaCertificadoidCategoriaCertificado = categoriaCertificadoidCategoriaCertificado;
        changeSupport.firePropertyChange("categoriaCertificadoidCategoriaCertificado", oldCategoriaCertificadoidCategoriaCertificado, categoriaCertificadoidCategoriaCertificado);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubCategoriaCertificado != null ? idSubCategoriaCertificado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subcategoriacertificado)) {
            return false;
        }
        Subcategoriacertificado other = (Subcategoriacertificado) object;
        if ((this.idSubCategoriaCertificado == null && other.idSubCategoriaCertificado != null) || (this.idSubCategoriaCertificado != null && !this.idSubCategoriaCertificado.equals(other.idSubCategoriaCertificado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "View.Subcategoriacertificado[ idSubCategoriaCertificado=" + idSubCategoriaCertificado + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
