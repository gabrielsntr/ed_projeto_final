/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author GabrielBuosi
 */
@Entity
@Table(name = "categoriacertificado", catalog = "trabalhofinal", schema = "")
@NamedQueries({
    @NamedQuery(name = "Categoriacertificado.findAll", query = "SELECT c FROM Categoriacertificado c")
    , @NamedQuery(name = "Categoriacertificado.findByIdCategoriaCertificado", query = "SELECT c FROM Categoriacertificado c WHERE c.idCategoriaCertificado = :idCategoriaCertificado")
    , @NamedQuery(name = "Categoriacertificado.findByNome", query = "SELECT c FROM Categoriacertificado c WHERE c.nome = :nome")
    , @NamedQuery(name = "Categoriacertificado.findByFixa", query = "SELECT c FROM Categoriacertificado c WHERE c.fixa = :fixa")
    , @NamedQuery(name = "Categoriacertificado.findByHorasMaximoTotal", query = "SELECT c FROM Categoriacertificado c WHERE c.horasMaximoTotal = :horasMaximoTotal")})
public class Categoriacertificado implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCategoriaCertificado")
    private Integer idCategoriaCertificado;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "fixa")
    private boolean fixa;
    @Basic(optional = false)
    @Column(name = "horasMaximoTotal")
    private float horasMaximoTotal;

    public Categoriacertificado() {
    }

    public Categoriacertificado(Integer idCategoriaCertificado) {
        this.idCategoriaCertificado = idCategoriaCertificado;
    }

    public Categoriacertificado(Integer idCategoriaCertificado, String nome, boolean fixa, float horasMaximoTotal) {
        this.idCategoriaCertificado = idCategoriaCertificado;
        this.nome = nome;
        this.fixa = fixa;
        this.horasMaximoTotal = horasMaximoTotal;
    }

    public Integer getIdCategoriaCertificado() {
        return idCategoriaCertificado;
    }

    public void setIdCategoriaCertificado(Integer idCategoriaCertificado) {
        Integer oldIdCategoriaCertificado = this.idCategoriaCertificado;
        this.idCategoriaCertificado = idCategoriaCertificado;
        changeSupport.firePropertyChange("idCategoriaCertificado", oldIdCategoriaCertificado, idCategoriaCertificado);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        String oldNome = this.nome;
        this.nome = nome;
        changeSupport.firePropertyChange("nome", oldNome, nome);
    }

    public boolean getFixa() {
        return fixa;
    }

    public void setFixa(boolean fixa) {
        boolean oldFixa = this.fixa;
        this.fixa = fixa;
        changeSupport.firePropertyChange("fixa", oldFixa, fixa);
    }

    public float getHorasMaximoTotal() {
        return horasMaximoTotal;
    }

    public void setHorasMaximoTotal(float horasMaximoTotal) {
        float oldHorasMaximoTotal = this.horasMaximoTotal;
        this.horasMaximoTotal = horasMaximoTotal;
        changeSupport.firePropertyChange("horasMaximoTotal", oldHorasMaximoTotal, horasMaximoTotal);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCategoriaCertificado != null ? idCategoriaCertificado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoriacertificado)) {
            return false;
        }
        Categoriacertificado other = (Categoriacertificado) object;
        if ((this.idCategoriaCertificado == null && other.idCategoriaCertificado != null) || (this.idCategoriaCertificado != null && !this.idCategoriaCertificado.equals(other.idCategoriaCertificado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "View.Categoriacertificado[ idCategoriaCertificado=" + idCategoriaCertificado + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
