-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 08-Dez-2018 às 18:58
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trabalhofinal`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

DROP TABLE IF EXISTS `aluno`;
CREATE TABLE IF NOT EXISTS `aluno` (
  `idAluno` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario_idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idAluno`),
  KEY `fk_Aluno_Usuario1_idx` (`Usuario_idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `aluno`
--

INSERT INTO `aluno` (`idAluno`, `Usuario_idUsuario`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoriacertificado`
--

DROP TABLE IF EXISTS `categoriacertificado`;
CREATE TABLE IF NOT EXISTS `categoriacertificado` (
  `idCategoriaCertificado` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `fixa` tinyint(1) NOT NULL,
  `horasMaximoTotal` float NOT NULL,
  PRIMARY KEY (`idCategoriaCertificado`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categoriacertificado`
--

INSERT INTO `categoriacertificado` (`idCategoriaCertificado`, `nome`, `fixa`, `horasMaximoTotal`) VALUES
(1, 'Participação em eventos', 0, 40),
(2, 'Atividades de Extensão', 0, 40),
(3, 'Atividades de Pesquisa', 0, 40),
(4, 'Atividade de Ensino', 0, 30),
(5, 'Publicações', 0, 40),
(6, 'Participação em órgãos colegiados', 0, 20),
(7, 'Atuação em Programas e Núcleos Temáticos', 0, 30),
(8, 'Disciplinas extracurricular es cursadas', 0, 30),
(9, 'Disciplinas cursadas na modalidade eletiva livre', 0, 30),
(10, 'Outras Atividades', 0, 30);

-- --------------------------------------------------------

--
-- Estrutura da tabela `certificado`
--

DROP TABLE IF EXISTS `certificado`;
CREATE TABLE IF NOT EXISTS `certificado` (
  `idCertificado` int(11) NOT NULL AUTO_INCREMENT,
  `Aluno_idAluno` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `dataEnvio` date NOT NULL,
  `dataEmissao` date DEFAULT NULL,
  `horasValidas` float DEFAULT NULL,
  `cargaHoraria` float NOT NULL,
  `comentario` varchar(140) DEFAULT NULL,
  `CategoriaCertificado_idCategoriaCertificado` int(11) NOT NULL,
  `SubCategoriaCertificado_idSubCategoriaCertificado` int(11) NOT NULL,
  `CriteriosLocalTempo_idCriteriosLocalTempo` int(11) NOT NULL,
  `caminhoImagem` varchar(180) DEFAULT NULL,
  PRIMARY KEY (`idCertificado`),
  KEY `fk_Certificado_Aluno1_idx` (`Aluno_idAluno`),
  KEY `fk_Certificado_CategoriaCertificado1_idx` (`CategoriaCertificado_idCategoriaCertificado`),
  KEY `fk_Certificado_SubCategoriaCertificado1_idx` (`SubCategoriaCertificado_idSubCategoriaCertificado`),
  KEY `fk_Certificado_CriteriosLocalTempo1_idx` (`CriteriosLocalTempo_idCriteriosLocalTempo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `coordenador`
--

DROP TABLE IF EXISTS `coordenador`;
CREATE TABLE IF NOT EXISTS `coordenador` (
  `idCoordenador` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario_idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idCoordenador`),
  KEY `fk_Coordenador_Usuario_idx` (`Usuario_idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `coordenador`
--

INSERT INTO `coordenador` (`idCoordenador`, `Usuario_idUsuario`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `criterioslocaltempo`
--

DROP TABLE IF EXISTS `criterioslocaltempo`;
CREATE TABLE IF NOT EXISTS `criterioslocaltempo` (
  `idCriteriosLocalTempo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  `RegraDeHoras_idRegraDeHoras` int(11) NOT NULL,
  `SubCategoriaCertificado_idSubCategoriaCertificado` int(11) NOT NULL,
  PRIMARY KEY (`idCriteriosLocalTempo`),
  KEY `fk_CriteriosLocalTempo_RegraDeHoras1_idx` (`RegraDeHoras_idRegraDeHoras`),
  KEY `fk_CriteriosLocalTempo_SubCategoriaCertificado1_idx` (`SubCategoriaCertificado_idSubCategoriaCertificado`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `criterioslocaltempo`
--

INSERT INTO `criterioslocaltempo` (`idCriteriosLocalTempo`, `nome`, `RegraDeHoras_idRegraDeHoras`, `SubCategoriaCertificado_idSubCategoriaCertificado`) VALUES
(1, 'Evento local', 0, 1),
(2, 'Evento regional', 0, 1),
(3, 'Evento nacional', 1, 1),
(4, 'Evento internacional', 1, 1),
(5, 'Evento local', 1, 2),
(6, 'Evento regional', 1, 2),
(7, 'Evento nacional', 2, 2),
(8, 'Evento internacional', 4, 2),
(9, 'Por semestre', 4, 3),
(10, 'Por evento', 4, 4),
(11, 'Por curso', 4, 5),
(12, 'Por curso', 3, 6),
(13, 'Por semestre', 4, 7),
(14, 'Por semestre', 2, 8),
(15, 'Periódicos indexados pela Capes – A1 a B2 (aceite)', 9, 9),
(16, 'Periódicos indexados pela Capes – B3 a C (aceite)', 8, 9),
(17, 'Periódicos não indexados pela Capes (aceite)', 4, 9),
(18, 'Anais de congresso internacional', 4, 9),
(19, 'Anais de congresso nacional', 2, 9),
(20, 'Anais de congresso regional ou iniciação científica', 1, 9),
(21, 'Por semestre', 1, 10),
(22, 'Por semestre', 1, 11),
(23, 'A cada 30 horas cursadas', 5, 12),
(24, 'A cada 30 horas cursadas', 5, 13),
(25, 'A cada 30 horas cursadas', 5, 14),
(98, 'A cada 30 horas cursadas', 5, 15),
(99, 'A cada 30 horas cursadas', 5, 16),
(100, 'A cada 30 horas cursadas', 5, 17),
(101, 'No município, por visita', 7, 18),
(102, 'Na região (raio de até 250km), por visita', 6, 18),
(103, 'No estado (raio maior de 250km), por visita', 6, 18),
(104, 'Fora do estado, por visita', 1, 18),
(105, 'Por palestra', 7, 19),
(106, 'Por protótipo', 1, 20),
(107, 'Por idioma', 2, 21),
(108, 'Por distinção ou mérito', 2, 22);

-- --------------------------------------------------------

--
-- Estrutura da tabela `regradehoras`
--

DROP TABLE IF EXISTS `regradehoras`;
CREATE TABLE IF NOT EXISTS `regradehoras` (
  `idRegraDeHoras` int(11) NOT NULL AUTO_INCREMENT,
  `cargaHorariaMinima` float DEFAULT NULL,
  `cargaHorariaMaxima` float DEFAULT NULL,
  PRIMARY KEY (`idRegraDeHoras`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `regradehoras`
--

INSERT INTO `regradehoras` (`idRegraDeHoras`, `cargaHorariaMinima`, `cargaHorariaMaxima`) VALUES
(0, 0, 6),
(1, 0, 10),
(2, 0, 15),
(3, 0, 8),
(4, 0, 20),
(5, 0, 7.5),
(6, 0, 5),
(7, 0, 3),
(8, 0, 25),
(9, 0, 30);

-- --------------------------------------------------------

--
-- Estrutura da tabela `subcategoriacertificado`
--

DROP TABLE IF EXISTS `subcategoriacertificado`;
CREATE TABLE IF NOT EXISTS `subcategoriacertificado` (
  `idSubCategoriaCertificado` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(140) DEFAULT NULL,
  `CategoriaCertificado_idCategoriaCertificado` int(11) NOT NULL,
  PRIMARY KEY (`idSubCategoriaCertificado`),
  KEY `fk_SubCategoriaCertificado_CategoriaCertificado1_idx` (`CategoriaCertificado_idCategoriaCertificado`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `subcategoriacertificado`
--

INSERT INTO `subcategoriacertificado` (`idSubCategoriaCertificado`, `nome`, `CategoriaCertificado_idCategoriaCertificado`) VALUES
(1, 'Ouvinte: Participação em seminários, semanas acadêmicas, congressos (Iniciação Científica ou não) e simpósios', 1),
(2, 'Apresentador: Participação em seminários, semanas acadêmicas, congressos (Iniciação Científica ou não) e simpósios', 1),
(3, 'Participação em projetos de extensão ', 2),
(4, 'Organização de eventos ', 2),
(5, 'Cursos Ministrados', 2),
(6, 'Minicursos Ministrados ', 2),
(7, 'Iniciação científica – participação em projetos de pesquisa (bolsista ou não)', 3),
(8, 'Monitoria voluntária ou subsidiada', 4),
(9, 'null', 5),
(10, 'null2', 6),
(11, 'Programa PET, Empresa Júnior, Grupo de Estudos, Bolsa EAD e demais bolsas acadêmicas', 7),
(12, 'Disciplinas eletivas, quando excedentes ao número de créditos eletivos exigidos pelo Curso, cursadas com aproveitamento', 8),
(13, 'Disciplinas adicionais cursadas com aproveitamento ', 8),
(14, 'Disciplinas obrigatórias alternativas excedentes às exigidas pelo currículo, cursadas com aproveitamento', 8),
(15, 'Disciplinas de outros cursos da UNEMAT cursadas com aproveitamento', 8),
(16, 'Disciplinas relacionadas ao Curso cursadas com aproveitamento em outra IES, nacional ou estrangeira, sem duplicidade de aproveitamento', 8),
(17, 'Disciplinas de outros cursos da UNEMAT, Campus de Sinop, cursadas com aproveitamento', 9),
(18, 'Visitas técnicas institucionais (exceto aulas práticas de disciplinas de graduação)', 10),
(19, 'Palestras fora de eventos ', 10),
(20, 'Desenvolvimento de Protótipos ', 10),
(21, 'Aprovação em exame de suficiência ou proficiência em idioma estrangeiro', 10),
(22, 'Distinções e méritos acadêmicos', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `senha` varchar(45) NOT NULL,
  `dataNascimento` date NOT NULL,
  `email` varchar(45) NOT NULL,
  `turma` varchar(45) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nome`, `cpf`, `senha`, `dataNascimento`, `email`, `turma`) VALUES
(1, 'Gabriel Alan Buosi', '123', '123', '1995-12-27', 'gabriel_buosi_@hotmail.com', '2017/1'),
(2, 'Generico Aluno', '321', '123', '1233-12-27', 'aluno@unemat.br', '2017/1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `validacao`
--

DROP TABLE IF EXISTS `validacao`;
CREATE TABLE IF NOT EXISTS `validacao` (
  `idValidacao` int(11) NOT NULL AUTO_INCREMENT,
  `Coordenador_idCoordenador` int(11) NOT NULL,
  `Certificado_idCertificado` int(11) NOT NULL,
  PRIMARY KEY (`idValidacao`),
  KEY `fk_Validacao_Coordenador1_idx` (`Coordenador_idCoordenador`),
  KEY `fk_Validacao_Certificado1_idx` (`Certificado_idCertificado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `aluno`
--
ALTER TABLE `aluno`
  ADD CONSTRAINT `fk_Aluno_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `certificado`
--
ALTER TABLE `certificado`
  ADD CONSTRAINT `fk_Certificado_Aluno1` FOREIGN KEY (`Aluno_idAluno`) REFERENCES `aluno` (`idAluno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Certificado_CategoriaCertificado1` FOREIGN KEY (`CategoriaCertificado_idCategoriaCertificado`) REFERENCES `categoriacertificado` (`idCategoriaCertificado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Certificado_CriteriosLocalTempo1` FOREIGN KEY (`CriteriosLocalTempo_idCriteriosLocalTempo`) REFERENCES `criterioslocaltempo` (`idCriteriosLocalTempo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Certificado_SubCategoriaCertificado1` FOREIGN KEY (`SubCategoriaCertificado_idSubCategoriaCertificado`) REFERENCES `subcategoriacertificado` (`idSubCategoriaCertificado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `coordenador`
--
ALTER TABLE `coordenador`
  ADD CONSTRAINT `fk_Coordenador_Usuario` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `criterioslocaltempo`
--
ALTER TABLE `criterioslocaltempo`
  ADD CONSTRAINT `fk_CriteriosLocalTempo_RegraDeHoras1` FOREIGN KEY (`RegraDeHoras_idRegraDeHoras`) REFERENCES `regradehoras` (`idRegraDeHoras`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_CriteriosLocalTempo_SubCategoriaCertificado1` FOREIGN KEY (`SubCategoriaCertificado_idSubCategoriaCertificado`) REFERENCES `subcategoriacertificado` (`idSubCategoriaCertificado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `subcategoriacertificado`
--
ALTER TABLE `subcategoriacertificado`
  ADD CONSTRAINT `fk_SubCategoriaCertificado_CategoriaCertificado1` FOREIGN KEY (`CategoriaCertificado_idCategoriaCertificado`) REFERENCES `categoriacertificado` (`idCategoriaCertificado`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `validacao`
--
ALTER TABLE `validacao`
  ADD CONSTRAINT `fk_Validacao_Certificado1` FOREIGN KEY (`Certificado_idCertificado`) REFERENCES `certificado` (`idCertificado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Validacao_Coordenador1` FOREIGN KEY (`Coordenador_idCoordenador`) REFERENCES `coordenador` (`idCoordenador`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
